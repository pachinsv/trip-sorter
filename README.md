# TripSorter

## Installation
1. Clone repository
2. Move to directory where the repository was cloned
3. Install depedencies ```composer install```

Note: If the project was installed from the archive file, there's no need to install dependencies,
since all dependencies are included in the archive.

## Run test
1. Move to project directory.
2. Execute the following command

```bash
./bin/phpunit --color Tests/
```


## How to extend to support additional transportation means
1. Add a class in the namespace TripSorter\BoardingCard ```e.g. TrainBoardingCard```,
extending from the class BoardingCard.
2. Implement 2 methods defined in the interface (build, instruction).
