<?php

require_once __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use TripSorter\BoardingCard\AirplaneBoardingCard;

class AirplaneBoardingCardTest extends TestCase
{
    public function testCanHandleGateParameter()
    {
        $bc = new AirplaneBoardingCard("a", "b", "55B", "10A", "23B");
        $this->assertEquals("a", $bc->from());
        $this->assertEquals("b", $bc->to());
        $this->assertEquals("55B", $bc->number());
        $this->assertEquals("10A", $bc->gate());
        $this->assertEquals("23B", $bc->seat());
    }
}
