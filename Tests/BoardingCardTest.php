<?php

require_once __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use TripSorter\BoardingCard\BusBoardingCard;

class BoardingCardTest extends TestCase
{
    public function testCanBeInitialized()
    {
        $bc = new BusBoardingCard('Barcelona', 'Madrid', 0, "4B");
        $this->assertEquals("Barcelona", $bc->from());
        $this->assertEquals("Madrid", $bc->to());
        $this->assertEquals(0, $bc->number());
        $this->assertEquals("4B", $bc->seat());
    }

    public function testCanDetermineIfCardIsNextStop()
    {
        $card1 = new BusBoardingCard('a', 'b', 1);
        $card2 = new BusBoardingCard('b', 'c', 2);

        $this->assertTrue($card1->nextStop($card2));
    }

    public function testCanDetermineIfCardIsNotNextStop() {
        $card1 = new BusBoardingCard('b', 'c', 1);
        $card2 = new BusBoardingCard('a', 'b', 2);

        $this->assertFalse($card1->nextStop($card2));
    }

}
