<?php

require_once __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use TripSorter\BoardingCard\BusBoardingCard;
use TripSorter\Sorter;

/**
 *
 */
class SorterTest extends TestCase
{
    public function testCanBeInitializedWithAllTheCards()
    {
        $boardingCards = [new BusBoardingCard("a", "b", 1), new BusBoardingCard("c", "d", 2)];
        $sorter = new Sorter($boardingCards);
        $this->assertEquals($boardingCards, $sorter->cards());
    }

    public function testCardsCanBeAdded()
    {
        $boardingCard1 = new BusBoardingCard("a", "b", 1);
        $boardingCard2 = new BusBoardingCard("b", "c", 1);

        $sorter = new Sorter();
        $sorter->addCard($boardingCard1);
        $sorter->addCard($boardingCard2);

        $this->assertCount(2, $sorter->cards());
    }

    public function testCanSortUnorderedCards()
    {
        $cards = [
            new BusBoardingCard('a', 'b', 1),
            new BusBoardingCard('b', 'c', 2),
            new BusBoardingCard('c', 'd', 3),
        ];

        $sorter = new Sorter();
        $sorter->addCard($cards[2]);
        $sorter->addCard($cards[1]);
        $sorter->addCard($cards[0]);

        $this->assertEquals($cards, $sorter->sort());
    }

    public function testCanSortOrderedCards()
    {
        $cards = [
            new BusBoardingCard('a', 'b', 1),
            new BusBoardingCard('b', 'c', 2),
            new BusBoardingCard('c', 'd', 3),
        ];

        $sorter = new Sorter();
        $sorter->addCard($cards[0]);
        $sorter->addCard($cards[1]);
        $sorter->addCard($cards[2]);

        $this->assertEquals($cards, $sorter->sort());
    }

    public function testCanDetermineTheFirstCard()
    {
        $cards = [
            new BusBoardingCard('a', 'b', 1),
            new BusBoardingCard('b', 'c', 2),
            new BusBoardingCard('c', 'd', 3),
        ];

        $sorter = new Sorter();
        $sorter->addCard($cards[2]);
        $sorter->addCard($cards[0]);
        $sorter->addCard($cards[1]);

        $this->assertEquals($cards[0], $sorter->firstCard());
    }

    // public function testGetInstructions()
    // {
    //     $cards = [
    //         new BusBoardingCard('a', 'b', 1),
    //         new BusBoardingCard('b', 'c', 2),
    //         new BusBoardingCard('c', 'd', 3),
    //     ];
    //
    //     $sorter = new Sorter();
    //     $sorter->addCard($cards[2]);
    //     $sorter->addCard($cards[0]);
    //     $sorter->addCard($cards[1]);
    //
    //     $this->assertEquals($cards[0], $sorter->firstCard());
    // }
}
