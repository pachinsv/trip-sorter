<?php

require_once __DIR__.'/vendor/autoload.php';

use TripSorter\Sorter;
use TripSorter\BoardingCard\BusBoardingCard;
use TripSorter\BoardingCard\AirplaneBoardingCard;


$sorter = new Sorter([
    new AirplaneBoardingCard('Dubai', 'Los Angeles', "13", "1A", "10A"),
    new AirplaneBoardingCard('Amsterdam', 'Dubai', "11", "2B", "20B"),
    new AirplaneBoardingCard('Panama', 'Amsterdam', "22", "3C", "30C"),
    new BusBoardingCard('San Salvador', 'Panama', "44", "1A"),
]);

foreach($sorter->instructions() as $key => $instruction) {
    echo $key + 1 . ". $instruction\n";
}
