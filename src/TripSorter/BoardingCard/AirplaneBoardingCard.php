<?php

namespace TripSorter\BoardingCard;

class AirplaneBoardingCard extends BoardingCard
{
    private $gate;

    public function __construct($from, $to, $number, $gate, $seat = null)
    {
        $this->gate = $gate;
        parent::__construct($from, $to, $number, $seat);
    }

    public function build()
    {
        $this->setMean('Airplane');
    }

    public function gate()
    {
        return $this->gate;
    }

    public function setGate($gate)
    {
        $this->gate = $gate;
    }

    public function instruction()
    {
        return sprintf("From %s airport take flight %s to %s. Gate %s, seat %s",
            $this->from(),
            $this->number(),
            $this->to(),
            $this->gate(),
            $this->seat()
        );
    }
}
