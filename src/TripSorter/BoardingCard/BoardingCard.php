<?php

namespace TripSorter\BoardingCard;

require_once __DIR__.'/../../../vendor/autoload.php';

abstract class BoardingCard implements BoardingCardInterface
{
    private $number;
    private $seat;
    private $from;
    private $to;
    private $mean;

    public function __construct($from, $to, $number, $seat = null)
    {
        $this->from = $from;
        $this->to = $to;
        $this->number = $number;
        $this->seat = $seat;

        $this->build();
    }

    public function __toString()
    {
        return sprintf("\n%s -> %s", $this->from, $this->to);
    }

    public function nextStop(BoardingCard $card)
    {
        return strcasecmp($this->to(), $card->from()) == 0 ? true: false;
    }

    public function number()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function seat()
    {
        return $this->seat;
    }

    public function setSeat($seat)
    {
        $this->seat = $seat;
    }

    public function from()
    {
        return $this->from;
    }

    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function to()
    {
        return $this->to;
    }

    public function setTo($to)
    {
        $this->to = $to;
    }

    public function mean()
    {
        return $this->mean;
    }

    public function setMean($mean)
    {
        $this->mean = $mean;
    }
}
