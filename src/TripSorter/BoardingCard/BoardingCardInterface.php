<?php

namespace TripSorter\BoardingCard;

interface BoardingCardInterface {
    public function instruction();
    public function build();
}
