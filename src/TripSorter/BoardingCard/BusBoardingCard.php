<?php

namespace TripSorter\BoardingCard;

class BusBoardingCard extends BoardingCard
{
    public function build()
    {
        $this->setMean('Bus');
    }

    public function instruction()
    {
        $message = sprintf("Take the bus %s from %s to %s. ",
            $this->number(),
            $this->from(),
            $this->to()
        );

        if($this->seat()) {
            $message .= "Seat in seat {$this->seat()}";
        } else {
            $message .= "No seat assignment.";
        }

        return $message;
    }
}
