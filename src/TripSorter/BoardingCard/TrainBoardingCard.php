<?php

namespace TripSorter\BoardingCard;

class TrainBoardingCard extends BoardingCard
{
    public function build()
    {
        $this->setMean('Train');
    }

    public function instruction()
    {
        return sprintf("Take train %s from %s to %s. Sit in seat %s.",
            $this->number(),
            $this->from(),
            $this->to(),
            $this->seat()
        );
    }
}
