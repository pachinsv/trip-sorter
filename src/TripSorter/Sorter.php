<?php

namespace TripSorter;

use TripSorter\BoardingCard\BoardingCardInterface;

class Sorter
{
    private $cards;
    private $sorted;

    /**
     * Inicilizes the Sorter object.
     *
     * @param array $cards Simple array of objects implementing the BoardingCardInterface.
     */
    public function __construct(array $cards = null)
    {
        $this->cards = $cards;
    }

    public function cards()
    {
        return $this->cards;
    }

    /**
     * Adds a boarding card to the collection.
     *
     * @param BoardingCardInterface $card A boarding card object.
     */
    public function addCard(BoardingCardInterface $card)
    {
        $this->cards[] = $card;
    }

    /**
     * Retrieves the first card (starting point of the trip).
     *
     * @return BoardingCardInterface A boarding card object.
     */
    public function firstCard()
    {
        $fromList = [];
        $toList = [];

        foreach ($this->cards() as $card) {
            $fromList[] = $card->from();
            $toList[] = $card->to();
        }

        $origin = array_diff($fromList, $toList);

        // Use array_pop because the index could be not zero based.
        $origin = array_pop($origin);

        foreach ($this->cards() as $card) {
            if ($card->from() == $origin) {
                return $card;
            }
        }
    }

    /**
     * Sorts the list of bording cards.
     *
     * @return array array of sorted boarding cards.
     */
    public function sort()
    {
        $cards = $this->cards();
        $firstCard = $this->firstCard();
        $ordered = [$firstCard];
        $to = $firstCard->to();

        $cards = array_diff($cards, $ordered);
        $cards = array_values($cards);

        for ($i = 0; $i < count($cards); $i++) {
            $card = $cards[$i];
            if ($card->from() == $to) {
                $ordered[] = $card;
                $to = $card->to();
                $i = -1;
            }
        }

        $this->sorted = true;
        return $ordered;
    }

    /**
     * Generates instructions
     *
     * @return array List of instruction.
     */
    public function instructions()
    {
        $instructions = [];

        foreach ($this->sort() as $card) {
            $instructions[] = $card->instruction();
        }

        $instructions[] = "You have arrived at your final destination.";

        return $instructions;
    }

    public function sorted()
    {
        return $this->sorted;
    }

}
